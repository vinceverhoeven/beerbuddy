app.directive('navButton', function ($location) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {

            (function () {
                var url = $location.path().substring(1);
                var hash = element[0].hash.substring(1);
                if (hash == url) {
                    addActive();
                }
            })();

            element.bind('click', function (e) {
                addActive();
            });

            function addActive() {
                element.parent().find("a").removeClass("active");
                element.addClass('active');
            }
        }
    };
});

app.directive('gameDirective', function (gameFactory) {
    return {
        restrict: 'A',
        transclude: true,
        link: function (scope, element, attr) {
            gameFactory.createCanvas(element[0]);
            gameFactory.createDeviceOrientation(function (e) {
                // DEBUG
                // scope.$apply(function () {
                //     scope.alpha = e.alpha;
                // });
            });
        }
    };
});