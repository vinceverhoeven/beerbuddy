app.controller('homeController', function ($scope, $state, homeFactory, gameFactory, $stateParams) {
    $scope.owner = homeFactory.getOwner();
    $scope.progressBar = homeFactory.getProgressBar();
    homeFactory.checkProgressBarRules();
    $scope.addBeerToday = function () {
        homeFactory.updateOwner();
    };
    $scope.$watch('owner.promile', function (newValue, oldValue) {
        if (newValue !== oldValue) {
            if (gameFactory.startGame()) {
                gameFactory.continueAnimate();
                $state.go('game');
            }
        }
    });
    console.log($stateParams);
    if($stateParams.gameFailed){
        console.log('gamefaulked');
    }
});

app.controller('standController', function ($scope, standFactory, $ionicPopup) {
    standFactory.calculateStand();
    $scope.$watch('owner.promile', function (newValue, oldValue) {
        if (newValue !== oldValue) {
            standFactory.calculateStand();
        }
    });

    $scope.resetPromille = function () {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Alcohol promile op 0',
            template: 'Wil je dat de Bier van vandaag en alcohol promile wordt gereset?'
        });
        confirmPopup.then(function (res) {
            if (res) {
                standFactory.resetPromille();
            } else {
            }
        });
    };

    $scope.resetAll = function () {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Reset drank probleem',
            template: 'Wil je dat alles wordt gereset, inclusief all time?'
        });
        confirmPopup.then(function (res) {
            if (res) {
                standFactory.resetAll();
            } else {
            }
        });
    };
});


app.controller('socialShareController', function ($scope, $cordovaSocialSharing, homeFactory) {
    var owner = homeFactory.getOwner();
    var message = "BeerBuddy " + owner.beerXP + " xp";

    $scope.whatsAppShare = function () {
        $cordovaSocialSharing
            .shareViaWhatsApp(message, "www/img/bier-icon-rond.png")
            .then(function (result) {
                // Success!
                alert('Bedankt voor het delen!')
            }, function (err) {
                // An error occurred. Show a message to the user
                alert('kon geen verbinding maken')
            });
    };

    $scope.twitterShare = function () {
        $cordovaSocialSharing
            .shareViaTwitter(message, "www/img/bier-icon-rond.png")
            .then(function (result) {
                // Success!
                alert('Bedankt voor het delen!')
            }, function (err) {
                // An error occurred. Show a message to the user
                alert('error')
            });
    }
});
