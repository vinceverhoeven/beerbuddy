/**
 * Created by vince on 12/7/2016.
 */
app.factory('sessionFactory', ['$http', function ($http, $window) {
    return {
        set: function (key, value) {
            return localStorage.setItem(key, JSON.stringify(value));
        },
        get: function (key) {
            return JSON.parse(localStorage.getItem(key));
        },
        destroy: function (key) {
            return localStorage.removeItem(key);
        },
        clear: function () {
            localStorage.clear();
        }
    };
}]);

app.factory('homeFactory', function (homeService, $timeout, sessionFactory) {
    this.getOwner = function () {
        homeService.owner.beerToday = sessionFactory.get('beer') || 0;
        homeService.owner.promile = sessionFactory.get('promile') || 0;
        homeService.owner.beerXP = sessionFactory.get('beerXP') || 0;
        homeService.progressBar.width = sessionFactory.get('progressBarWidth') || "0%";
        homeService.owner.statusToday = sessionFactory.get('statusToday') || homeService.owner.statusToday;

        homeService.owner.statusAllTime.beer = sessionFactory.get('allTimeBeer') || 0;
        homeService.owner.statusAllTime.drunk = sessionFactory.get('allTimeDrunk') || homeService.owner.statusAllTime.drunk || 0;
        homeService.owner.statusAllTime.tipsy = sessionFactory.get('allTimeTipsy') || homeService.owner.statusAllTime.tipsy || 0;
        homeService.owner.statusAllTime.story = sessionFactory.get('allTimeStory') || homeService.status.allTime["0"].status;
        homeService.owner.statusAllTime.medal = sessionFactory.get('allTimeMedal') || homeService.status.medal["0"].status;

        homeService.owner.statusAllTime.literBeer = sessionFactory.get('allTimeLiterBeer') || 0;
        ;
        return homeService.owner;
    };

    this.resetStorage = function () {
        sessionFactory.destroy('beer');
        sessionFactory.destroy('promile');
        sessionFactory.destroy('allTimeBeer');
    };

    this.allowedToPress = function () {
        if (homeService.rules.allowedToPress) {
            homeService.rules.allowedToPress = false;
            $timeout(function () {
                homeService.rules.allowedToPress = true;
            }, homeService.rules.beerPressTime);
            return true;
        }
    };

    this.setBeer = function () {
        homeService.owner.beerToday += 1;
        sessionFactory.set('beer', homeService.owner.beerToday);
    };
    this.getProgressBar = function () {
        return homeService.progressBar;
    };
    this.setBeerXP = function () {
        homeService.owner.beerXP += 10;
        var int = parseInt(homeService.progressBar.width, 10);
        homeService.progressBar.width = int + 10 + '%';
        sessionFactory.set('beerXP', homeService.owner.beerXP);
        sessionFactory.set('progressBarWidth', homeService.progressBar.width);
        this.checkProgressBarRules();
    };
    this.checkProgressBarRules = function () {
        var int = parseInt(homeService.progressBar.width, 10);
        if (int > 100) {
            homeService.progressBar.width = "0%";
        }
    };
    this.timePassed = function () {
        if (!homeService.owner.timeFirstBeer) {
            homeService.owner.timeFirstBeer = new Date();
        }
        // later record end time
        var endTime = new Date();
        // time difference in ms
        var timeDiff = endTime - homeService.owner.timeFirstBeer;
        // strip the ms
        timeDiff /= 1000;

        // remove seconds from the date
        timeDiff = Math.floor(timeDiff / 60);

        // remove minutes from the date
        timeDiff = Math.floor(timeDiff / 60);

        // remove hours from the date
        timeDiff = Math.floor(timeDiff / 24);

        homeService.owner.timePassed = Math.round(timeDiff % 24);
    };

    this.resetTime = function () {
        // reset today beers
        // reset promile
        // reset time
        // reset statustoday
    };

    this.calculatePromile = function () {
        //   BAG (1 x 10)/(70 x 0.6) - (1h - 0,5) x (70 x 0,002)
        homeService.owner.promile = (homeService.owner.beerToday * 10) / (70 * 0.6) - (homeService.owner.timePassed - 0.5) * (70 * 0.002);
        sessionFactory.set('promile', homeService.owner.promile);
    };

    this.setBeerAlltime = function () {
        homeService.owner.statusAllTime.beer += 1;
        sessionFactory.set('allTimeBeer', homeService.owner.statusAllTime.beer);
    };

    this.setTipsyAlltime = function () {
        if (homeService.owner.promile > homeService.rules.tipsyPromile) {
            homeService.owner.statusAllTime.tipsy += 1;
            sessionFactory.set('allTimeTipsy', homeService.owner.statusAllTime.tipsy);
        }
    };
    this.setLiterAlLTime = function () {
        homeService.owner.statusAllTime.literBeer += 0.333;
        sessionFactory.set('allTimeLiterBeer', homeService.owner.statusAllTime.literBeer);
    };
    this.setDrunkAlltime = function () {
        if (homeService.owner.promile > homeService.rules.drunkPromile) {
            homeService.owner.statusAllTime.drunk += 1;
            sessionFactory.set('allTimeDrunk', homeService.owner.statusAllTime.drunk);
        }
    };
    this.setStoryAllTime = function () {
        for (var key in homeService.status["allTime"]) {
            if (key <= homeService.owner.statusAllTime.beer) {
                var story = homeService.status["allTime"][key].status;
            }
        }
        homeService.owner.statusAllTime.story = story;
        sessionFactory.set('allTimeStory', homeService.owner.statusAllTime.story);
    };
    this.setMedalAllTime = function () {
        for (var key in homeService.status["medal"]) {
            if (key <= homeService.owner.statusAllTime.beer) {
                var story = homeService.status["medal"][key].status;
            }
        }
        homeService.owner.statusAllTime.medal = story;
        sessionFactory.set('allTimeMedal', homeService.owner.statusAllTime.medal);
    };
    this.setStatusToday = function () {
        for (var key in homeService.status["today"]) {
            if (key <= homeService.owner.beerToday) {
                var story = homeService.status["today"][key].status;
            }
        }
        homeService.owner.statusToday = story;
        sessionFactory.set('statusToday', homeService.owner.statusToday);
    };
    this.updateOwner = function () {
        if (this.allowedToPress()) {
            this.setBeer(); // eerst
            this.setBeerXP();
            this.setBeerAlltime(); // eerst
            this.setTipsyAlltime();
            this.setDrunkAlltime();
            this.timePassed();
            this.setStoryAllTime();
            this.setMedalAllTime();
            this.setLiterAlLTime();
            this.setStatusToday();
            this.calculatePromile();
        }
    };
    return this;
});


app.factory('gameFactory', function (homeService, $window, $state) {

        var beerImage = new Image();
        beerImage.src = homeService.game.beerImageSrc;
        var that = this;
        var game;
        var continueAnimating = true;

        this.startGame = function () {
            var kans = Math.floor(Math.random() * 2) + 1;
            if (kans == 2 && homeService.owner.promile > homeService.rules.tipsyPromile)
                return true;
        };

        this.createCanvas = function (canvas) {
            homeService.game.canvas = canvas;
            homeService.game.context = canvas.getContext("2d");

            canvas.width = 100;
            canvas.height = $window.innerHeight - 120;
            this.resetBeer();
        };
        this.resetBeer = function () {
            this.gameOver();
            homeService.game.beerY = homeService.game.canvas.height - 50;
            homeService.game.beerX = 30
            homeService.game.failed += 1
        };
        this.gameOver = function () {
            if (homeService.game.failed >= 3) {
                homeService.game.failed = 0;
                homeService.game.continue = false;
                $state.go('home').then(function () {
                    alert('Oi Drinker you failed. -10 xp');
                    homeService.owner.beerXP -= 10;
                    alert('Remember to move your phone vertically and rotate it while screen is towards you ');
                });
            }
        };
        this.createDeviceOrientation = function (callback) {
            if (window.DeviceOrientationEvent) {
                window.addEventListener("deviceorientation", function (e) {
                    homeService.game.alpha = e.alpha;
                    callback(e);
                }, false);
            } else {
                alert("DeviceOrientationEvent is not supported");
            }
        };
        this.drawBeer = function () {
            homeService.game.context.drawImage(
                beerImage, // image
                homeService.game.beerX, // xpos
                homeService.game.beerY, // ypos
                homeService.game.beerWidth, // width
                homeService.game.beerHeight // height
            );
        };
        this.moveBeer = function () {
            homeService.game.beerY -= homeService.game.speedY;
        };
        this.steerBeer = function () {
            if (homeService.game.straight < homeService.game.alpha) {
                homeService.game.beerX -= homeService.game.speedX;
            }
            if (homeService.game.straight > homeService.game.alpha) {
                homeService.game.beerX += homeService.game.speedX;
            }
        };
        this.finishGame = function () {
            if (homeService.game.beerY < 20) {
                this.resetBeer();
                homeService.game.continue = false;
                $state.go('home');
            }
        };
        this.border = function () {
            if (homeService.game.beerX > homeService.game.canvas.width - homeService.game.beerWidth) {
                this.resetBeer();
            }
            if (homeService.game.beerX < 0) {
                this.resetBeer();
            }
        };
        this.animate = function () {
            if (homeService.game.continue) {
                homeService.game.context.clearRect(
                    0, 0,
                    homeService.game.canvas.width,
                    homeService.game.canvas.height
                );
                console.log('game running');
                that.drawBeer();
                that.moveBeer();
                that.border();
                that.steerBeer();
                that.finishGame();
                requestAnimationFrame(that.animate);
            }
        };
        this.continueAnimate = function () {
            requestAnimationFrame(that.animate);
            homeService.game.continue = true;
        };

        return this;
    }
);

app.factory('standFactory', function (homeService, sessionFactory) {

    this.getPromile = function () {
        return homeService.owner.promile
    };
    this.calculateStand = function () {
        this.getPromile();
        this.getTipsyPercentage();
        this.getDrunkPercentage();
        this.getCanDrive();
        this.getDriveTicket();
        this.getMililiterBeer();
        this.getLiverProces();
        this.getCanBike();
        this.getCanWalk();
    };
    this.getTipsyPercentage = function () {
        var promile = this.getPromile();
        homeService.owner.tipsyPercentage =
            ( promile / homeService.rules.tipsyPromile ) * 100;
    };
    this.getDrunkPercentage = function () {
        var promile = this.getPromile();
        homeService.owner.drunkPercentage =
            ( promile / homeService.rules.drunkPromile ) * 100;
    };
    this.getCanDrive = function () {
        var promile = this.getPromile();
        if (homeService.rules.canDrivePromile > promile) {
            homeService.owner.canDrive = "yes";
        } else {
            homeService.owner.canDrive = "no";
        }
    };
    this.getDriveTicket = function () {
        for (var key in homeService.boetes.today) {
            if (key <= this.getPromile()) {
                var value = homeService.boetes.today[key];
            }
        }
        homeService.owner.driveTicket = value;
    };
    this.getMililiterBeer = function () {
        homeService.owner.mililiterBeer = homeService.owner.beerToday * 0.333;
    };
    this.getLiverProces = function () {
        // 1 uur over 1 bier
        homeService.owner.liverProces = homeService.owner.beerToday * 60;
    };
    this.getCanWalk = function () {
        for (var key in homeService.walkSentence) {
            if (key <= this.getPromile()) {
                var value = homeService.walkSentence[key];
            }
        }
        homeService.owner.canWalk = value;
    };
    this.getCanBike = function () {
        for (var key in homeService.bikeSentence) {
            if (key <= this.getPromile()) {
                var value = homeService.bikeSentence[key];
            }
        }
        homeService.owner.canBike = value;
    };

    this.resetPromille = function () {
        homeService.owner.beerToday = 0
        homeService.owner.promile = 0
        sessionFactory.destroy('promile');
        sessionFactory.destroy('beer');
    };
    this.resetAll = function () {
        sessionFactory.clear();
    };

    return this;
});










