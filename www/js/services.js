app.service('homeService', function () {
    this.owner = {
        "beerXP": 0,
        "beerToday": 0,
        "statusToday": 'Do you even like beer?',
        "timeFirstBeer": null,
        "timePassed": null,
        "promile": 0,
        "tipsyPercentage": 0,
        "drunkPercentage": 0,
        "canDrive": "yes",
        "driveTicket": 0,
        "mililiterBeer": 0,
        "liverProces": 0,
        "canBike": "",
        "canWalk": "",
        "statusAllTime": {
            "medal": "",
            "story": "",
            "beer": 0,
            "tipsy": 0,
            "drunk": 0,
            "literBeer": 0
        }
    };
    this.progressBar = {
        "width": "0%"
    };
    this.rules = {
        "canDrivePromile": 0.4,
        "beerPressTime": 5000,
        "allowedToPress": true,
        "tipsyPromile": 0.6,
        "drunkPromile": 1.5,
        "progressBar": {}
    };
    this.game = {
        'gameStarted': null,
        'alpha': 0,
        'beerX': 30,
        'beerY': 400,
        'beerWidth': 50,
        'beerHeight': 50,
        'beerImageSrc': 'img/bier-afb.png',
        'canvas': null,
        'context': null,
        'speedY': 1,
        'continue': false,
        'speedX': 0.3,
        'straight': 150,
        'failed' : 0
    };
    this.boetes = {
        "today": {
            0: "",
            0.51: "360",
            0.81: "500 + lema-cursus",
            1: "600  + ema-cursus",
            1.15: "750  + ema-cursus",
            1.30: "Rechter + ASP"
        }
    };
    this.walkSentence = {
        0: "Easy",
        0.81: "picobello",
        1: "floating",
        1.30: "crawling"
    };
    this.bikeSentence = {
        0: "yes",
        0.81: "worth trying",
        1: "good luck",
        1.30: "find it first"
    };
    this.status = {
        "today": {
            "0": {
                "status": "Do you even like Beer?"
            },
            "1": {
                "status": "It is not a lemonade?"
            },
            "2": {
                "status": "Still laughed at"
            },
            "3": {
                "status": "Keep it up"
            },
            "4": {
                "status": "Is getting cozier"
            },
            "5": {
                "status": "Drink this one fast"
            },
            "6": {
                "status": "Beer makes you strong"
            },
            "7": {
                "status": "Crawling home"
            },
            "8": {
                "status": "Old droppings"
            }
        },
        "medal": {
            "0": {
                "status": "Lemonade drinker"
            }
            ,
            "3": {
                "status": "Explorer"
            }
            ,
            "5": {
                "status": "Drinking along"
            }
            ,
            "8": {
                "status": "Beginner"
            }
            ,
            "15": {
                "status": "Promillage shiner"
            }
            ,
            "22": {
                "status": "Alcohol swekker"
            }
            ,
            "35": {
                "status": "Professional drinker"
            }
        }
        ,
        "allTime": {
            "0": {
                "status": "Your friends doubt whether you have ever drunk beer. Such a friendship never exists for long."
            }
            ,
            "3": {
                "status": "According to experts, you do not like beer."
            }
            ,
            "5": {
                "status": "You drink, but do you enjoy too?"
            }
            ,
            "8": {
                "status": "You get recognition. However, it is on a thin string."
            }
            ,
            "15": {
                "status": "The recognition draws in wide surroundings. People talk behind your back about your performance."
            }
            ,
            "22": {
                "status": "The alcohol composition has caused your beer to attract the attention of many."
            }
            ,
            "35": {
                "status": "The application forms have been sent to the addiction clinic."
            }
        }
    }
    ;
});



