app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'templates/home.html'
        })
        .state('game', {
            url: '/game',
            templateUrl: 'templates/game.html'
        })
        .state('current_stats', {
            url: '/current_stats',
            templateUrl: 'templates/current_stats.html'
        })
        .state('all_time_stats', {
            url: '/all_time_stats',
            templateUrl: 'templates/all_time_stats.html'
        });
    $urlRouterProvider.otherwise("/home");
});
